# 911SafeMapApp
A very very early beta and very much still in development app for Android and iOS. It is designed to not take users home the fastest way, but instead the safest way. It pulls real time violence reported to 911 and user reported violence to establish color-coded zones to show safe routes of travel and get you from point A to point B as safe and fast as possible.
